﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Roulette_jabanashvili.Data;
using Roulette_jabanashvili.Helper.Auth;
using Roulette_jabanashvili.Helper.Logger;
using Roulette_jabanashvili.Models;

namespace Roulette_jabanashvili.Controllers
{
    [Auth]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private ApplicationDbContext context;
        IConfiguration configuration;

        public UserController(ApplicationDbContext context, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;

        }


        [HttpPost]
        [Route("getinfo")]
        public IActionResult GetInfo()
        {
            try
            {
                //Get username and balance from database
                AppUser user = (from t in context.SessionTokens
                                join usr in context.AppUsers on t.AppUser.ID equals usr.ID
                                where t.Token == getToken()
                                select new AppUser()
                                {
                                    UserName = usr.UserName,
                                    Balance = usr.Balance

                                }).ToList().FirstOrDefault();

                //if user object is null then return http status code 500 (internal server error)
                if (user == null)
                {
                    return StatusCode(500);
                }
                return Ok(new { username = user.UserName, balance = user.Balance });
            }
            catch (Exception ex)
            {
                Log.Logger("GetInfo:" + ex.Message);
            }

            return StatusCode(500);
        }

        protected string getToken()
        {
            if (Request.Query.ContainsKey("token"))
            {
                return Request.Query.Where(x => x.Key == "token").FirstOrDefault().Value;
            }
            return "";
        }

    }
}