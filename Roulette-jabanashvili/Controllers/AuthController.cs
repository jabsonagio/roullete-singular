﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Roulette_jabanashvili.Data;
using Roulette_jabanashvili.Helper.Crypto;
using Roulette_jabanashvili.Helper.Logger;
using Roulette_jabanashvili.Models;

namespace Roulette_jabanashvili.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        private ApplicationDbContext context;
        IConfiguration configuration;

        public AuthController(ApplicationDbContext context, IConfiguration configuration)
        {
            this.context = context;
            this.configuration = configuration;
        }

        [HttpGet]
        public string Index()
        {
            //Veeery very special text!!!
            return "Welcome to REST API";
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] Login model)
        {

            try
            {
                //Check Username and Password
                var user = context.AppUsers.Where(x => x.UserName == model.UserName && x.Password == Cryptography.HmacSHA256(model.UserName, model.Password)).FirstOrDefault();

                if (user != null)
                {
                    //Get sign key from appsettings.json
                    string key = configuration.GetSection("SignKey").Value;
                    //concat sign string Username:Password:Time, time because every token will be unic
                    string data = String.Format("{0}:{1}:{2}", user.UserName, user.Password, DateTime.Now.ToLongTimeString());
                    //Generate token
                    string tokenValue = Cryptography.HmacSHA256(key, data);
                    //Save in Db
                    context.SessionTokens.Add(new SessionToken() { AppUser = user, Token = tokenValue, CreateDate = DateTime.Now, LastRequest = DateTime.Now });
                    context.SaveChanges();

                    return Ok(new { token = tokenValue, Username = user.UserName, Balance = user.Balance });
                }
                return Unauthorized();
            }
            catch (Exception ex)
            {
                Log.Logger("Login:"+ex.Message);
                return Unauthorized();
            }
        }
    }
}