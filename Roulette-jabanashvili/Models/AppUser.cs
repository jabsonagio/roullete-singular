﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette_jabanashvili.Models
{
    public class AppUser
    {

        public int ID { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public long Balance { get; set; }

        public ICollection<Spin> spins { get; set; }

        public ICollection<SessionToken> Tokens { get; set; }

    }
}
