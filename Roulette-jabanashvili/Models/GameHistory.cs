﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette_jabanashvili.Models
{
    public class GameHistory
    {
        public int SpinId { get; set; }

        public long BetAmount { get; set; }

        public long WonAmount { get; set; }

        public string Date { get; set; }
    }
}
