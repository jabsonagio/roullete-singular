﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette_jabanashvili.Models
{
    public class Spin
    {
        [Key]
        public int ID { get; set; }

        public long BetAmount { get; set; }

        public long WonAmount { get; set; }

        public int WinningNumber { get; set; }

        public DateTime SpinDate { get; set; }

        public string IpAddress { get; set; }

        public AppUser AppUser { get; set; }

        public SessionToken SessionToken { get; set; }

        public long AmountForJackpot { get; set; }


    }
}
