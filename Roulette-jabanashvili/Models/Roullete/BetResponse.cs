﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette_jabanashvili.Models.Roullete
{
    public class BetResponse
    {
        public int SpinId { get; set; }

        public string Status { get; set; }

        public long Amount { get; set; }

        public int WinningNumber { get; set; }
    }
}
