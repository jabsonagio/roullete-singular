﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette_jabanashvili.Models.Roullete
{
    //Class for Jack, who sit in pot
    public class JackInPot
    {
        public int ID { get; set; }

        public long JackPotAmount { get; set; }

        public int Status { get; set; }
    }
}
