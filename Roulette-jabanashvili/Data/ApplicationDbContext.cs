﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Roulette_jabanashvili.Models;
using Roulette_jabanashvili.Models.Roullete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette_jabanashvili.Data
{
    public class ApplicationDbContext: DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options):base(options)
        {

        }

        public DbSet<Spin> Spins { get; set; }


        public DbSet<AppUser> AppUsers { get; set; }

        public DbSet<SessionToken> SessionTokens { get; set; }

        public DbSet<JackInPot> JackinPots { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Spin>().HasOne(e => e.AppUser).WithMany(e => e.spins);
            builder.Entity<Spin>().HasOne(x => x.SessionToken).WithMany(e => e.Spins);
            builder.Entity<SessionToken>().HasOne(e => e.AppUser).WithMany(e => e.Tokens);
        }
    }
}
