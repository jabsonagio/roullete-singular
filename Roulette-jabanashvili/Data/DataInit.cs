﻿using Microsoft.Extensions.DependencyInjection;
using Roulette_jabanashvili.Helper.Crypto;
using Roulette_jabanashvili.Models;
using Roulette_jabanashvili.Models.Roullete;
using System;
using System.Linq;

namespace Roulette_jabanashvili.Data
{
    public class DataInit
    {
        
        public static void AddData(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<ApplicationDbContext>();
            context.Database.EnsureCreated();

            if (!context.AppUsers.Any()) {

                AppUser user = new AppUser()
                {
                    UserName = "Singular",
                    Password = Cryptography.HmacSHA256("Singular", "Singul@r123"),
                    Balance = 1000
                };

                context.AppUsers.Add(user);

                JackInPot jackInPot = new JackInPot();
                jackInPot.JackPotAmount = 0;
                jackInPot.Status = 1;
                context.JackinPots.Add(jackInPot);

                context.SaveChangesAsync();

            }

        }
    }
}
