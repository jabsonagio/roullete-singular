﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Roulette_jabanashvili.Data;
using Roulette_jabanashvili.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime;
using System.Threading.Tasks;

namespace Roulette_jabanashvili.Helper.Auth
{
    //Create Custom Authorize class
    [AttributeUsage(AttributeTargets.Class)]
    public class Auth : Attribute, IAuthorizationFilter
    {
        //Just 5 minutes for session
        TimeSpan SessionTime = new TimeSpan(0, 5, 0); 

        public Auth()
        {
            

        }

        //Check token validation
        public void OnAuthorization(AuthorizationFilterContext context)
        {

            try
            {

                bool jackpot = false;
                //Get Db context
                var dbContext = (ApplicationDbContext)context.HttpContext.RequestServices.GetService(typeof(ApplicationDbContext));
                //Get token from http object from request
                string token = context.HttpContext.Request.Query.Where(x => x.Key == "token").FirstOrDefault().Value;

                if (context.HttpContext.Request.Query.ContainsKey("jackpot"))
                {
                    jackpot =  bool.Parse(context.HttpContext.Request.Query.Where(x => x.Key == "jackpot").FirstOrDefault().Value);
                }
                 

                if (token != null)
                {
                    SessionToken sessionToken = dbContext.SessionTokens.Where(s => s.Token == token).FirstOrDefault();
                    if (sessionToken != null)
                    {
                        //Calculate difference from last active
                        TimeSpan diff = DateTime.Now - sessionToken.LastRequest;
                        //Check if user inactive about 5 minutes long
                        if (SessionTime > diff)
                        {
                            if (!jackpot)
                            {
                                //If he/she is active then update last request date
                                sessionToken.LastRequest = DateTime.Now;
                                dbContext.SessionTokens.Update(sessionToken);
                                dbContext.SaveChanges();
                            }
                            
                            return;
                        }
                    }
                }

                //If there is no token and also user inactive, then send UnauthorizedResult status
                context.Result = new UnauthorizedResult();
                return;
            }
            catch (Exception)
            {
                context.Result = new UnauthorizedResult();
                return;
            }
            
        }
    }
}

