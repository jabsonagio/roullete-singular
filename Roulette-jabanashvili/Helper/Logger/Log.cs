﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Roulette_jabanashvili.Helper.Logger
{
    public static class Log
    {
        public static void Logger(string message)
        {
            string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string logFile = DateTime.Now.ToString("yyyyMMdd") + ".txt";
            string fullPath = String.Format("{0}\\Log-{1}", directory, logFile);
            if (!System.IO.File.Exists(fullPath))
            {
                var myFile = System.IO.File.Create(fullPath);
                myFile.Close();
            }

           File.AppendAllText(fullPath, DateTime.Now.ToString() + "-" + message + Environment.NewLine);
        }

    }
}
