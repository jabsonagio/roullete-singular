import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { AppComponent } from '../app.component';
import { RoulleteService } from '../services/roullete.service';
import { DOCUMENT } from '@angular/common'; 
import { Bets } from '../models/bets';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { JackpotService } from '../services/jackpot.service';
import { error } from 'protractor';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  showGameHistory = false;
  rouletteCircle;
  coinHover;
  gameHistory;
  mainTable;
  step = 9.72972972972973;
  rotation = 0;
  numbers = [0, 32, 15, 19, 4, 21, 2, 25, 17, 34, 6, 27, 13, 36, 11, 30, 8, 23, 10, 5, 24, 16, 33, 1, 20, 14, 31, 9, 22, 18, 29, 7, 28, 12, 35, 3, 26];
  arrayIndex = 0;
  randomCircle = 0;
  speed = 5;
  winNum = 0;
  timer;
  config = {
    fade: true,
    alwaysOn: false,
    neverOn: false,

    // fill
    fill: true,
    fillColor: '#ffffff',
    fillOpacity: 0.4,

    // stroke
    stroke: true,
    strokeColor: '#4d0ec0',
    strokeOpacity: 1,
    strokeWidth: 1,

    // shadow:
    shadow: true,
    shadowColor: '#000000',
    shadowOpacity: 0.8,
    shadowRadius: 10
  }

  Bets: Bets[];
  CurrentBetAmount=100;
  user:User;
  totalBet = 0;
  winAmoun = 0;
  tempWinAmount = 0;
  blockActions = false;
  displayWinningNum =0;
  errorResult = false;
  errorMessage = false;
  jackpotAmount = 0;

  constructor(private appComponent:AppComponent,private roullete: RoulleteService,private userService:UserService,private jackPot:JackpotService,@Inject(DOCUMENT) document) { }


  ngOnInit(): void {
    this.rouletteCircle = document.getElementById('rouletteCircle');
    this.coinHover = document.getElementById('coinHover');
    this.mainTable = document.getElementById('mainTable');
    this.Bets = [];
    this.user = new User();
    this.initUserInfo();
    this.getJackpot();
  }

  
  initUserInfo(){
    this.userService.getInfo().subscribe(
      data => {
        this.user.username = data.username;
        this.user.balance = data.balance;
      },
      error => {
        if(error.status == 401){
          this.appComponent.authorize = false;
          localStorage.clear();
        }
      }
    );
  }

  getJackpot(){
    this.timer = setInterval(() => {
      this.jackPot.getJackpot().subscribe(data => {
        this.jackpotAmount = data["jackpotAmount"]/100;
      },
      error=>{
        if(!error.ok){
          clearInterval(this.timer);
          this.logout();
        }
      });
    }, 2000);
    
  }

  gamehistory(){
    this.roullete.gameHistory().subscribe(
      data => {
        this.gameHistory = data;
        this.showGameHistory=true;
      },
      error => {
        if(error.status == 401){
          this.appComponent.authorize = false;
          localStorage.clear();
        }
      }
    );
  }

  closeGameHistory(){
    this.showGameHistory=false;
  }

  logout(){
    localStorage.clear();
    this.appComponent.authorize = false;
  }

  Spin() {


    if(this.Bets.length == 0){
      this.displayError("Make your bet please");
      return;
    }

    if(!this.blockActions){
      this.blockActions = true;
      this.roullete.spin(this.Bets).subscribe(data => {
        this.winNum = data.WinningNumber;
        this.tempWinAmount = data.Amount/100;
      },
      error => {
        if(error.status == 401){
          this.appComponent.authorize = false;localStorage.clear();
          localStorage.clear();
        }
      });
      this.randomCircle = this.getRandomInt(10,15);
      this.timer = setInterval(() => {
        this.oneStep(); 
      }, this.speed);
    }else{
      this.displayError("Loading...");
    }

  }

  clearBet(returnMoney){
    this.removeElementsByClass("coin");

    if(returnMoney){
      for(let i=0;i<this.Bets.length;i++){
        this.user.balance +=this.Bets[i].Amount;
      }
    }
    
    while(this.Bets.length > 0) {
      this.Bets.pop();
    }
    this.totalBet = 0;
  }
  
  //Roullete circle one step
  oneStep() {
    this.rouletteCircle.style.transform = 'rotate(' + this.rotation + 'deg)';
    
    if (this.numbers[this.arrayIndex] == this.winNum) {
      this.randomCircle--;

        if (this.randomCircle == 0) {
            clearInterval(this.timer);
            this.clearBet(false);
            this.removeElementsByClass("coin");
            this.totalBet = 0;
            this.winAmoun = this.tempWinAmount;
            this.blockActions = false;
            this.user.balance +=this.tempWinAmount*100;
            this.displayWinningNum = this.winNum;
        }
    }

    this.arrayIndex++

    if (this.arrayIndex > 36) this.arrayIndex = 0;

    this.rotation -= this.step;
  }

  getRandomInt(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  changeBet(bet){
    this.CurrentBetAmount = bet;
    switch(bet) {
      case 100:
        this.coinHover.style.marginLeft = "615px";
        break;
      case 200:
        this.coinHover.style.marginLeft = "665px";
        break;
      case 500:
        this.coinHover.style.marginLeft = "715px";
        break;
      case 1000:
        this.coinHover.style.marginLeft = "765px";
        break;
      default:
        this.coinHover.style.marginLeft = "615px";
    }
  }


  //bet combination red,black,odd...
  beting(type,e: MouseEvent){

    try{
      if(this.user.balance == 0){
        this.displayError("No balance");
        return;
      }
  
        for(let i=0;i<this.Bets.length;i++){
          if(this.Bets[i].Type == type){
            let string = this.checkRange(type,this.Bets[i].Amount+this.CurrentBetAmount);
            if(string !=""){
                this.displayError(string);
              return;
            }
            
            this.user.balance -= this.CurrentBetAmount;
            this.Bets[i].Amount +=this.CurrentBetAmount;
            this.totalBet +=this.CurrentBetAmount/100;
            this.createCoin(e.x-20,e.y-60);
            return;
          }
      }
  
      let string = this.checkRange(type,this.CurrentBetAmount);
      if(string !=""){
        this.displayError(string);
        return;
      }
  
      this.user.balance -= this.CurrentBetAmount;
      this.totalBet +=this.CurrentBetAmount/100;
  
      let Item = new Bets();
      Item.Amount = this.CurrentBetAmount;
      Item.Number = 0;
      Item.Type = type;
      this.Bets.push(Item);
      this.createCoin(e.x-20,e.y-60);
    }catch{
      this.logout();
    }
   
  }

  //bet inly numbers
  betingNum(number,e: MouseEvent){
    
    try{
      if(this.user.balance == 0){
        this.displayError("No balance");
        return;
      }
  
      for(let i=0;i<this.Bets.length;i++){
          if(this.Bets[i].Number == number){
            let string = this.checkRange('N',this.Bets[i].Amount+this.CurrentBetAmount);
            if(string !=""){
              this.displayError(string);
              return;
            }
            this.user.balance -= this.CurrentBetAmount;
            this.Bets[i].Amount +=this.CurrentBetAmount;
            this.totalBet +=this.CurrentBetAmount/100;
            this.createCoin(e.x-20,e.y-60);
            return;
          }
      }
        let string = this.checkRange('N',this.CurrentBetAmount);
        if(string !=""){
          this.displayError(string);
          return;
        }
        let Item = new Bets();
        this.user.balance -= this.CurrentBetAmount;
        this.totalBet +=this.CurrentBetAmount/100;
        Item.Amount = this.CurrentBetAmount;
        Item.Number = number;
        Item.Type = 'N';
        this.Bets.push(Item);
        this.createCoin(e.x-20,e.y-60);
    }
    catch{
        this.logout();
    }
   

  }

  createCoin(x,y){
    let coin = document.createElement('div');
    coin.style.height = "30px";
    coin.style.width = "30px";
    coin.style.position = "absolute";
    coin.style.zIndex = "20000";
    coin.style.marginTop = y+"px";
    coin.style.marginLeft = x+"px";
    coin.style.borderRadius = "100%";
    coin.style.textAlign = "center";
    coin.style.fontWeight = "bold";
    coin.style.backgroundImage = "url('/assets/images/bet"+this.CurrentBetAmount/100+".png')";
    coin.style.backgroundSize = "30px 30px";
    coin.className = "coin";
    this.mainTable.appendChild(coin);
  }

  checkMinBet(type){
      switch (type)
      {
        case "N":
          return 100;
        case "ONECOLUMN":
        case "TWOCOLUMN":
        case "THREECOLUMN":
        case "ONEROW":
        case "TWOROW":
        case "THREEROW":
          return 500;
        case "FIRSTPART":
        case "SECONDPART":
        case "EVEN":
        case "RED":
        case "BLACK":
        case "ODD":
          return 1000;
        default:
          return 100;
      }
  }

  checkMaxBet(type){
    switch (type)
    {
      case "N":
        return 2000;
      case "ONECOLUMN":
      case "TWOCOLUMN":
      case "THREECOLUMN":
      case "ONEROW":
      case "TWOROW":
      case "THREEROW":
        return 10000;
      case "FIRSTPART":
      case "SECONDPART":
      case "EVEN":
      case "RED":
      case "BLACK":
      case "ODD":
        return 20000;
      default:
        return 0;
    }
  }

  checkRange(type,amount){
    if(amount < this.checkMinBet(type))
      return "Min amount:"+this.checkMinBet(type)/100;

    if(amount > this.checkMaxBet(type))
      return "Max amount:"+this.checkMaxBet(type)/100;


    return "";
  }

  removeElementsByClass(className){
    var elements = document.getElementsByClassName(className);
    while(elements.length > 0){
        elements[0].parentNode.removeChild(elements[0]);
    }
  }


  displayError(text){
    this.errorResult = true;
    this.errorMessage = text;
    let errorInterval = setInterval(() => {
      this.errorResult = false;
      clearInterval(errorInterval);
    }, 3000);
  }

}
