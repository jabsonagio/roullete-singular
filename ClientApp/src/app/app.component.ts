import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ClientApp';
  username;
  balance;
  authorize = false;
  ngOnInit(): void {
    if(localStorage.getItem("token") != null){
      this.authorize = true;
    }
  }

  
}
