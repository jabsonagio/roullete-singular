import { TestBed } from '@angular/core/testing';

import { RoulleteService } from './roullete.service';

describe('RoulleteService', () => {
  let service: RoulleteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoulleteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
