import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoulleteService {

  constructor(private http:HttpClient) { }

  gameHistory():Observable<any>{
    let tokenParam = new HttpParams();
    tokenParam = tokenParam.set('token', localStorage.getItem('token'));
    return this.http.post(`${baseUrl}roullete/gamehistory`,null,{params:tokenParam});
  }

  spin(data):Observable<any>{
    let tokenParam = new HttpParams();
    tokenParam = tokenParam.set('token', localStorage.getItem('token'));
    return this.http.post(`${baseUrl}roullete/spin`,data,{params:tokenParam});
  }
 
}