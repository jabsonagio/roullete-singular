import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { baseUrl } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JackpotService {

  constructor(private http:HttpClient) { }

  getJackpot(){
    let tokenParam = new HttpParams();
    tokenParam = tokenParam.set('token', localStorage.getItem('token'));
    tokenParam = tokenParam.set('jackpot', 'true');
    return this.http.post<string>(`${baseUrl}jackpot/`,null,{params:tokenParam});
  }
}
