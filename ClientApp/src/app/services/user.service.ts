import { HttpClientModule, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { baseUrl } from 'src/environments/environment';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

  getInfo(){
    let tokenParam = new HttpParams();
    tokenParam = tokenParam.set('token', localStorage.getItem('token'));
    return this.http.post<User>(`${baseUrl}user/getinfo`,null,{params:tokenParam});
  }

}
