import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {AppComponent} from '../app.component'
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formGroup:FormGroup;
  errorResult = false;
  
  constructor(private authservice: AuthService,private appComponent:AppComponent) { }

  ngOnInit(): void {
    this.initForm();
  }
  initForm(){
    this.formGroup = new FormGroup({
        username: new FormControl('Singular',[Validators.required]),
        password: new FormControl('Singul@r123',[Validators.required])
      })
  }

  login(){
    if(this.formGroup.valid){
      this.authservice.login(this.formGroup.value).subscribe(data=>{
          localStorage.setItem('token',data.token);
          this.appComponent.authorize = true;
          this.errorResult=false;
          this.appComponent.username = data.username;
          this.appComponent.balance = data.balance/100;
      },
      error=>{
          this.errorResult = true;
      });
    }
  }

}
